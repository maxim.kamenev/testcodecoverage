import { expect } from 'chai';
import chai from 'chai';
import chaiHttp from 'chai-http';
import faker from 'faker';
import server from '../../src/index';

chai.use(chaiHttp);

let quantity = 10;

describe('Addition of two numbers', () => {
    describe('GET /api/formulas/sum', () => {
        it(`Should sum ${quantity}`, (done) => {
            let operationCount = quantity;
            for (let i = 0; i < operationCount; i++) {
                const a = faker.random.number({ min: 2, max: 50 });
                const b = faker.random.number({ men: 5, max: 80 });
                chai
                    .request(server)
                    .get(`/api/formulas/sum?a=${a}&b=${b}`)
                    .end((err, res) => {
                        operationCount--;
                        expect(res).have.status(200);
                        expect(res.body).have.property('message');
                        expect(res.body.message).to.be.equal('Sum');
                        expect(res.body.message).to.be.an('string');
                        expect(res.body).have.property('sum');
                        expect(res.body.sum).to.be.an('number');
                        expect(Object.keys(res.body).length).to.deep.equal(2);
                        if (operationCount == 0) {
                            done();
                        }
                    });
            }
        });
    });
});

describe('Error checking when adding numbers and not numbers', () => {
    describe('GET /api/formulas/sum', () => {
        it(`Should get ${quantity} errors of folding two numbers`, (done) => {
            let operationCount = quantity;
            for (let i = 0; i < operationCount; i++) {
                const a = faker.random.number({ min: 2, max: 50 });
                const b = faker.random.arrayElement();
                chai
                    .request(server)
                    .get(`/api/formulas/sum?a=${a}&b=${b}`)
                    .end((err, res) => {
                        operationCount--;
                        expect(res).have.status(422);
                        expect(res.body).have.property('err');
                        expect(res.body.err).to.be.equal('NAN');
                        expect(res.body.err).to.be.an('string');
                        expect(Object.keys(res.body).length).to.deep.equal(1);
                        if (operationCount == 0) {
                            done();
                        }
                    });
            }
        });
    });
});

describe('Calculating factorial numbers', () => {
    describe('GET /api/formulas/factorial', () => {
        it(`Should ${quantity} factorial numbers`, (done) => {
            let operationCount = quantity;
            for (let i = 0; i < operationCount; i++) {
                const f = faker.random.number({ min: 2, max: 50 });
                chai
                    .request(server)
                    .get(`/api/formulas/factorial?f=${f}`)
                    .end((err, res) => {
                        operationCount--;
                        expect(res).have.status(200);
                        expect(res.body).have.property('message');
                        expect(res.body.message).to.be.equal('Factorial');
                        expect(res.body.message).to.be.an('string');
                        expect(res.body).have.property('factorial');
                        expect(res.body.factorial).to.be.an('number');
                        expect(Object.keys(res.body).length).to.deep.equal(2);
                        if (operationCount == 0) {
                            done();
                        }
                    });
            }
        });
    });
});

describe('Error while calculating factorial is not a numbers', () => {
    describe('GET /api/formulas/factorial', () => {
        it(`Should get ${quantity} factorial calculation errors`, (done) => {
            let operationCount = quantity;
            for (let i = 0; i < operationCount; i++) {
                const f = faker.random.arrayElement();
                chai
                    .request(server)
                    .get(`/api/formulas/factorial?f=${f}`)
                    .end((err, res) => {
                        operationCount--;
                        expect(res).have.status(422);
                        expect(res.body).have.property('err');
                        expect(res.body.err).to.be.equal('NAN');
                        expect(res.body.err).to.be.an('string');
                        expect(Object.keys(res.body).length).to.deep.equal(1);
                        if (operationCount == 0) {
                            done();
                        }
                    });
            }
        });
    });
});

describe('Calculate the area of a circle along the radius', () => {
    describe('GET /api/formulas/area', () => {
        it(`Should ${quantity} circle areas`, (done) => {
            let operationCount = quantity;
            for (let i = 0; i < operationCount; i++) {
                const r = faker.random.number({ min: 2, max: 50 });
                chai
                    .request(server)
                    .get(`/api/formulas/area?r=${r}`)
                    .end((err, res) => {
                        operationCount--;
                        expect(res).have.status(200);
                        expect(res.body).have.property('message');
                        expect(res.body.message).to.be.equal('Area');
                        expect(res.body.message).to.be.an('string');
                        expect(res.body).have.property('area');
                        expect(res.body.area).to.be.an('number');
                        expect(Object.keys(res.body).length).to.deep.equal(2);
                        if (operationCount == 0) {
                            done();
                        }
                    });
            }
        });
    });
});

describe('Error calculating the radius of a circle when the radius is not a number', () => {
    describe('GET /api/formulas/area', () => {
        it(`Should get ${quantity} lap radius calculation errors`, (done) => {
            let operationCount = quantity;
            for (let i = 0; i < operationCount; i++) {
                const r = faker.random.arrayElement();
                chai
                    .request(server)
                    .get(`/api/formulas/area?r=${r}`)
                    .end((err, res) => {
                        operationCount--;
                        expect(res).have.status(422);
                        expect(res.body).have.property('err');
                        expect(res.body.err).to.be.equal('NAN');
                        expect(res.body.err).to.be.an('string');
                        expect(Object.keys(res.body).length).to.deep.equal(1);
                        if (operationCount == 0) {
                            done();
                        }
                    });
            }
        });
    });
});
