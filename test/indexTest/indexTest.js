import { expect } from 'chai';
import chai from 'chai';
import chaiHttp from 'chai-http';
import server from '../../src/index';

chai.use(chaiHttp);

describe('Test the home page', () => {
    describe('GET /', () => {
        it('The route should work', (done) => {
            chai
                .request(server)
                .get(`/`)
                .end((err, res) => {
                    expect(res).have.status(200);
                    expect(res.body).have.property('message');
                    expect(res.body.message).to.be.equal(`Hello! It's me home page`);
                    expect(res.body.message).to.be.an('string');
                    expect(Object.keys(res.body).length).to.deep.equal(1);
                    done();
                });
        });
    });
});