import { expect } from 'chai';
import chai from 'chai';
import chaiHttp from 'chai-http';
import server from '../../src/index';

chai.use(chaiHttp);

describe('Test the initial route', () => {
    describe('GET /api', () => {
        it('The route should work', (done) => {
            chai
                .request(server)
                .get(`/api/`)
                .end((err, res) => {
                    expect(res).have.status(200);
                    expect(res.body).have.property('message');
                    expect(res.body.message).to.be.equal(`Hello! It's My API`);
                    expect(res.body.message).to.be.an('string');
                    expect(Object.keys(res.body).length).to.deep.equal(1);
                    done();
                });
        });
    });
});