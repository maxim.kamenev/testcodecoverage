// 1: NPM dependencies and imports.
import http from 'http'
import express from 'express'
import logger from 'morgan';
import bodyParser from 'body-parser';
import apiRoutes from './routes/routes';

// 2: router related modules.
// ... Nothing here, yet!

// 3: Initializations.
const port = process.env.PORT || 8082;
const router = express();
const server = http.createServer(router);

// 4: Parse as urlencoded and json.
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

// 5: Hook up the HTTP logger.
router.use(logger('dev'));

// 6: Set the static files location.
// ... Nothing here, yet!

// 7: Home route.
router.get('/', (req, res) => {
    return res.json({
        message: `Hello! It's me home page`
    });
});

// 8: Bundle API routes.
router.use('/api', apiRoutes);

// 9: Start the server.
server.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});

export { server };

export default router;
