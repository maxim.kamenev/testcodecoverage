import express from 'express';
import apiАormulas from '../controllers/formulas';

const router = express.Router();

router.get('/', (req, res) => {
    return res.json({
        message: `Hello! It's My API`,
    });
});

router.use('/formulas', apiАormulas);

export default router;