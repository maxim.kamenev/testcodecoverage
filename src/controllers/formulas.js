import express from 'express';

const router = express.Router();

//handles url http://localhost:8082/api/formulas/sum/
router.get('/sum', async (req, res) => {
    try {
        const a = req.query.a;
        const b = req.query.b;
        await checkForNumber(a, b);
        const result = await getSumTwoNumbers(a, b);
        return res.status(200).json({
            message: 'Sum',
            sum: result,
        });
    } catch (err) {
        return res.status(422).json({
            err: err.message,
        });
    };
});

function getSumTwoNumbers(a, b) {
    return parseInt(a) + parseInt(b);
};

//handles url http://localhost:8082/api/formulas/factorial/
router.get('/factorial', async (req, res) => {
    try {
        const f = req.query.f;
        await checkForNumber(f);
        const result = getFactorialNumbers(f);
        return res.status(200).json({
            message: 'Factorial',
            factorial: result,
        });
    } catch (err) {
        return res.status(422).json({
            err: err.message,
        });
    };
});

function getFactorialNumbers(f) {
    let result = 1;
    for (let i = 1; i <= f; i++) {
        result = result * i;
    };
    return result;
};

//handles url http://localhost:8082/api/formulas/area/
router.get('/area', async (req, res) => {
    try {
        const r = req.query.r;
        await checkForNumber(r);
        const result = getRadiusOfCircle(r);
        return res.status(200).json({
            message: 'Area',
            area: result,
        });
    } catch (err) {
        return res.status(422).json({
            err: err.message,
        });
    }
});

function getRadiusOfCircle(r) {
    return 3.1415 * (r ** 2);
};

export default router;

function checkForNumber(...data) {
    data.map(item => {
        if (isNaN(item)) {
            throw new Error('NAN');
        };
    });
};
